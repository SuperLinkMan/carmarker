//
//  MarkerController.swift
//  CarMarker
//
//  Created by Robert Patchett on 5/10/15.
//  Copyright © 2015 Robert Patchett. All rights reserved.
//

import Foundation
import CoreLocation
import WatchConnectivity

let CMLocationSetNotification = "CMLocationSetNotification"

class MarkerController: NSObject, CLLocationManagerDelegate, WCSessionDelegate {
    
    // MARK: Properties
    var markers = [String: Marker]()
    var currentMarker: Marker?
    var locationManager = CLLocationManager()
    
    
    // MARK: Funtions
    override init() {
        super.init()
        
        if WCSession.isSupported() {
            let session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }
    
        if let defaults = NSUserDefaults(suiteName: "group.carmarker") {
            if defaults.dictionaryForKey(markerDictKey) != nil {
                markers = unarchivedMarkers(defaults.dictionaryForKey(markerDictKey) as! [String: NSData])
                currentMarker = markers.values.maxElement()
            }
        }
    }
    
    private func saveMarkers() {
        // Save the markers dict
        if let defaults = NSUserDefaults(suiteName: "group.carmarker") {
            defaults.setObject(archiveMarkers(markers), forKey: markerDictKey)
        }
    }
    
    private func addMarkerToMarkers(marker: Marker?) {
        // Save the current marker to the markers dict
        if let marker = marker {
            markers.updateValue(marker, forKey: marker.startDate.description)
            saveMarkers()
            sendMarkerToiOS(marker)
        }
    }
    
    func saveOldPark() {
        // Save the old marker before making a new one (if one exists)
        if currentMarker != nil {
            if currentMarker!.endDate == nil {
                setEndDate()
            }
        }
    }
    
    func sendMarkerToiOS(marker: Marker) {
        if let dir: NSString = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.CachesDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first {
            let fileName = "marker"
            let path = dir.stringByAppendingPathComponent(fileName)
            let fileURL = NSURL(fileURLWithPath: path)
            let data = NSKeyedArchiver.archivedDataWithRootObject(marker)
            data.writeToURL(fileURL, atomically: true)
            
            WCSession.defaultSession().transferFile(fileURL, metadata: nil)
        }
    }
    
    func newPark(floor floor: Int?, row: Int?) {
        // Set marker
        saveOldPark()
        currentMarker = Marker(date: NSDate(), floor: floor, row: row)
        addMarkerToMarkers(currentMarker)
        
        startLocating()
    }
    
    func setEndDate() {
        currentMarker!.endDate = NSDate()
        addMarkerToMarkers(currentMarker)
    }
    
    func startLocating() {
        let status = CLLocationManager.authorizationStatus()
        locationManager.delegate = self
        if status == CLAuthorizationStatus.AuthorizedWhenInUse
        {
            locationManager.requestLocation()
        } else if status == CLAuthorizationStatus.NotDetermined && locationManager.respondsToSelector("requestAlwaysAuthorization") {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    deinit {
        if let defaults = NSUserDefaults(suiteName: "group.carmarker") {
            defaults.synchronize()
        }
    }
    
    
    // MARK: Location Manager Delegate
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let loc = locations.last as CLLocation!
        
        // Ensure the Marker object exists and can receive updates
        if currentMarker != nil {
            currentMarker!.location = loc
            addMarkerToMarkers(currentMarker)
            NSNotificationCenter.defaultCenter().postNotificationName(CMLocationSetNotification, object: self)
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        return
    }

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.AuthorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
}