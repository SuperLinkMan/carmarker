//
//  ViewController.swift
//  CarMarker
//
//  Created by Robert Patchett on 20/04/15.
//  Copyright (c) 2015 Robert Patchett. All rights reserved.
//

import CoreLocation
import UIKit
import WatchConnectivity

class ViewController: UIViewController, CLLocationManagerDelegate, WCSessionDelegate {

    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if WCSession.isSupported() {
            let session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let status = CLLocationManager.authorizationStatus()
        locationManager.delegate = self
        if (status == CLAuthorizationStatus.NotDetermined && locationManager.respondsToSelector("requestAlwaysAuthorization"))
        {
            locationManager.requestAlwaysAuthorization()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: WCSession Delegate
    func session(session: WCSession, didReceiveFile file: WCSessionFile) {
        if let data = NSData(contentsOfURL: file.fileURL) {
            if let defaults = NSUserDefaults(suiteName: "group.carmarker") {
                
                if let marker = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? Marker {
                    var markers = defaults.dictionaryForKey(markerDictKey) != nil
                        ? unarchivedMarkers(defaults.dictionaryForKey(markerDictKey) as! [String: NSData])
                        : [String: Marker]()
                    
                    markers.updateValue(marker, forKey: marker.startDate.description)
                    
                    defaults.setObject(archiveMarkers(markers), forKey: markerDictKey)
                }
            }
        }
    }
}

