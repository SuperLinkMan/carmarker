//
//  Marker.swift
//  CarMarker
//
//  Created by Robert Patchett on 18/05/15.
//  Copyright (c) 2015 Robert Patchett. All rights reserved.
//

import Foundation
import CoreLocation


// Consts for encoding/decoding
let startDateKey = "start date"
let endDateKey = "end date"
let locationKey = "location"
let floorKey = "floor"
let rowKey = "row"

func unarchivedMarkers(archivedMarkers: Dictionary<String, NSData>) -> Dictionary<String, Marker> {
    var markers = [String: Marker]()
    for (key, archivedMarker) in archivedMarkers {
        markers.updateValue(NSKeyedUnarchiver.unarchiveObjectWithData(archivedMarker) as! Marker, forKey: key)
    }
    
    return markers
}

func archiveMarkers(markers: Dictionary<String, Marker>) -> Dictionary<String, NSData> {
    var archivedMarkers = [String: NSData]()
    for (key, marker) in markers {
        archivedMarkers.updateValue(NSKeyedArchiver.archivedDataWithRootObject(marker), forKey: key)
    }
    
    return archivedMarkers
}

// MARK: Comparable protocol implementation
func ==(lhs: Marker, rhs: Marker) -> Bool {
    return lhs.startDate.compare(rhs.startDate) == .OrderedSame
}

func <(lhs: Marker, rhs: Marker) -> Bool {
    return lhs.startDate.compare(rhs.startDate) == .OrderedAscending
}

@objc(Marker) class Marker: NSObject, NSCoding, Comparable {
    
    private(set) var startDate: NSDate
    var endDate: NSDate?
    var location: CLLocation?
    private(set) var floor: Int?
    private(set) var row: Int?
    
    init(date: NSDate, floor: Int?, row: Int?) {
        self.startDate = date
        self.endDate = nil
        self.floor = floor
        self.row = row
        self.location = nil
    }
    
    // MARK: NSCoding protocol implementation
    @objc required init?(coder: NSCoder) {
        startDate = (coder.decodeObjectForKey(startDateKey) as? NSDate)!
        endDate = coder.decodeObjectForKey(endDateKey) as? NSDate
        location = coder.decodeObjectForKey(locationKey) as? CLLocation
        floor = coder.decodeObjectForKey(floorKey) as? Int
        row = coder.decodeObjectForKey(rowKey) as? Int
    }
    
    @objc func encodeWithCoder(encoder: NSCoder) {
        encoder.encodeObject(startDate, forKey:startDateKey)
        encoder.encodeObject(endDate, forKey:endDateKey)
        encoder.encodeObject(location, forKey:locationKey)
        encoder.encodeObject(floor, forKey:floorKey)
        encoder.encodeObject(row, forKey:rowKey)
    }
}
