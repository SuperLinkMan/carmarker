//
//  ParkingComplexInterfaceController.swift
//  CarMarker
//
//  Created by Robert Patchett on 25/05/15.
//  Copyright (c) 2015 Robert Patchett. All rights reserved.
//

import WatchKit

protocol ParkingComplexDelegate {
    func parkingComplexAttributesSet(floor floor: Int, row: Int)
}

class ParkingComplexInterfaceController: WKInterfaceController {
    
    let floorMin = -99
    let floorMax = 999
    let rowMin   = 0
    let rowMax   = 999
    
    var floorNumber = 0
    var rowNumber = 0
    let formator = ParkingComplexFormator()
    
    @IBOutlet weak var floorLabel: WKInterfaceLabel!
    @IBOutlet weak var rowLabel: WKInterfaceLabel!
    
    var delegate: ParkingComplexDelegate?
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        delegate = context as? ParkingComplexDelegate
    }
    
    @IBAction func floorMinus() {
        if floorNumber > floorMin
        {
            floorNumber--
            floorLabel.setText(formator.formatFloorNumber(floorNumber))
        }
    }
    
    @IBAction func floorPlus() {
        if floorNumber < floorMax
        {
            floorNumber++
            floorLabel.setText(formator.formatFloorNumber(floorNumber))
        }
    }
    
    @IBAction func rowMinus() {
        // Following Apple's designs, the buttons shouldn't be disabled, but instead not perform any function when not desired
        if rowNumber > rowMin
        {
            rowNumber--
            rowLabel.setText(formator.formatRowNumber(rowNumber))
        }
    }
    
    @IBAction func rowPlus() {
        if rowNumber < rowMax
        {
            rowNumber++
            rowLabel.setText(formator.formatRowNumber(rowNumber))
        }
    }
    
    @IBAction func setParkingComplexAttributies() {
        // Set marker
        if delegate != nil {
            delegate?.parkingComplexAttributesSet(floor: floorNumber, row: rowNumber)
        }

        dismissController()
    }
}
