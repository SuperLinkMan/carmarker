//
//  ParkingComplexFormator.swift
//  CarMarker
//
//  Created by Robert Patchett on 2/07/15.
//  Copyright (c) 2015 Robert Patchett. All rights reserved.
//

import Foundation

class ParkingComplexFormator {
    
    func formatFloorNumber(floorNumber:Int)->String {
        if floorNumber == 0
        {
            return NSLocalizedString("G", comment: "Ground floor")
        }
        else if floorNumber < 0
        {
            return NSLocalizedString("B", comment: "Below ground level prefix") +
                String(stringInterpolationSegment: (floorNumber*(-1)))
        }
        else
        {
            return String(stringInterpolationSegment: floorNumber)
        }
    }
    
    func formatRowNumber(rowNumber:Int)->String {
        var letter: String?
        switch rowNumber {
        case 0:
            letter = "–"
        case 1:
            letter = "A"
        case 2:
            letter = "B"
        case 3:
            letter = "C"
        case 4:
            letter = "D"
        case 5:
            letter = "E"
        case 6:
            letter = "F"
        case 7:
            letter = "G"
        case 8:
            letter = "H"
        case 9:
            letter = "I"
        case 10:
            letter = "J"
        case 11:
            letter = "K"
        case 12:
            letter = "L"
        case 13:
            letter = "M"
        case 14:
            letter = "N"
        case 15:
            letter = "O"
        case 16:
            letter = "P"
        case 17:
            letter = "Q"
        case 18:
            letter = "R"
        case 19:
            letter = "S"
        case 20:
            letter = "T"
        case 21:
            letter = "U"
        case 22:
            letter = "V"
        case 23:
            letter = "W"
        case 24:
            letter = "X"
        case 25:
            letter = "Y"
        case 26:
            letter = "Z"
        default:
            letter = nil
        }
        
        if letter != nil {
            if letter == "–" {
                return letter!
            } else {
                return letter! + " / " + String(stringInterpolationSegment: rowNumber)
            }
        } else {
            return String(stringInterpolationSegment: rowNumber)
        }
    }
}