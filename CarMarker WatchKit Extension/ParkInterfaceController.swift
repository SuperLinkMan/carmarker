//
//  ParkInterfaceController.swift
//  CarMarker WatchKit Extension
//
//  Created by Robert Patchett on 20/04/15.
//  Copyright (c) 2015 Robert Patchett. All rights reserved.
//

import WatchKit
import Foundation
import CoreLocation
import WatchConnectivity


class ParkInterfaceController: WKInterfaceController, CLLocationManagerDelegate, WCSessionDelegate, ParkingComplexDelegate {
    
    // MARK: Properties
    @IBOutlet weak var parkComplexGroup: WKInterfaceGroup!
    @IBOutlet weak var standardInterfaceGroup: WKInterfaceGroup!
    @IBOutlet weak var stopTimerButton: WKInterfaceButton!
    
    @IBOutlet weak var levelLabel: WKInterfaceLabel!
    @IBOutlet weak var rowLabel: WKInterfaceLabel!
    @IBOutlet weak var parkTimer: WKInterfaceTimer!
    @IBOutlet weak var parkMap: WKInterfaceMap!
    
    let formator = ParkingComplexFormator()
    
    var markerController: MarkerController?
    var notificationObserver: NSObjectProtocol?
    
    // MARK: Functions
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)

        if let context = context {
            markerController = context as? MarkerController
        }
        // Should be if (not else-if) incase as? MarkerController fails
        if markerController == nil {
            markerController = MarkerController()
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        notificationObserver = NSNotificationCenter.defaultCenter().addObserverForName(CMLocationSetNotification, object: nil, queue: nil, usingBlock: { (notification: NSNotification) in
            self.updateMap()
        })
        
        // Only show the map and other interface elements if the currentMarker is not nil
        if let currentMarker = markerController?.currentMarker
        {
            // Refresh the interface
            if currentMarker.endDate == nil {
                parkTimer.setDate(currentMarker.startDate)
                parkTimer.start()
            } else {
                parkTimer.setDate(NSDate(timeInterval: -currentMarker.endDate!.timeIntervalSinceNow, sinceDate: currentMarker.startDate))
            }
            
            if currentMarker.floor != nil {
                showParkingComplexInterface()
            } else {
                showSimpleParkInterface()
            }
            
            if currentMarker.location != nil {
                updateMap()
            }
        }
    }
    
    override func willDisappear() {
        if let notificationObserver = notificationObserver {
            NSNotificationCenter.defaultCenter().removeObserver(notificationObserver, name: CMLocationSetNotification, object: nil)
        }
        super.willDisappear()
    }
    
    @IBAction func newPark() {
        markerController!.newPark(floor: nil, row: nil)
        
        // Refresh the interface
        parkTimer.setDate(markerController!.currentMarker!.startDate)
        parkTimer.start()
        
        // This has to be done here as well as in willActivate because willActivate can execute before the new park is ready
        showSimpleParkInterface()
    }
    
    @IBAction func showParkComplexModal() {
        // Show parking complex interface
        presentControllerWithName("Park Complex Controller", context: self)
    }
    
    @IBAction func stopTimer() {
        assert(markerController!.currentMarker != nil, "currentMarker is nil but the stopTimer button is active.")
        
        markerController!.setEndDate()
        parkTimer.stop()
        showOrHideStopTimerButton()
    }
    
    func showSimpleParkInterface() {
        parkComplexGroup.setHidden(true)
        standardInterfaceGroup.setHidden(false)
        parkMap.setHidden(true)
        showOrHideStopTimerButton()
    }
    
    func showParkingComplexInterface() {
        assert(markerController!.currentMarker != nil, "currentMarker is nil but the interface is trying to display the parking complex interface (not the modal though).")
        assert(markerController!.currentMarker!.floor != nil && markerController!.currentMarker!.row != nil, "Either the level or row is not set even though the parking complex interface is selected.")
        
        // Set the park level and row to the user selected values
        levelLabel.setText(formator.formatFloorNumber(markerController!.currentMarker!.floor!))
    rowLabel.setText(formator.formatRowNumber(markerController!.currentMarker!.row!))
        
        showSimpleParkInterface()
        
        parkComplexGroup.setHidden(false)
    }
    
    func showOrHideStopTimerButton() {
        if let currentMarker = markerController!.currentMarker {
            stopTimerButton.setHidden(currentMarker.endDate != nil)
        }
    }
    
    func updateMap() {
        assert(markerController!.currentMarker != nil, "currentMarker is nil but the app is trying to update the map.")
        assert(markerController!.currentMarker!.location != nil, "No location set for Marker.")
        
        parkMap.setHidden(false)
        
        let pinViewOffset = 800.0
        let mapWidth = 5000.0
        var mapPoint = MKMapPointForCoordinate(markerController!.currentMarker!.location!.coordinate)
        mapPoint.x -= mapWidth/2
        mapPoint.y -= mapWidth/2 + pinViewOffset
        parkMap.removeAllAnnotations()
        parkMap.setVisibleMapRect(MKMapRect(origin: mapPoint, size: MKMapSize(width: mapWidth, height: mapWidth)))
        parkMap.addAnnotation(markerController!.currentMarker!.location!.coordinate, withPinColor: WKInterfaceMapPinColor.Red)
    }
    
    // MARK: Parking Complex Delegate
    func parkingComplexAttributesSet(floor floor: Int, row: Int) {
        // Create a new Marker
        markerController!.newPark(floor: floor, row: row)
    }
}
