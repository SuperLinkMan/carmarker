//
//  MainInterfaceController.swift
//  CarMarker
//
//  Created by Robert Patchett on 6/10/15.
//  Copyright © 2015 Robert Patchett. All rights reserved.
//

import WatchKit
import Foundation

class MainInterfaceController: WKInterfaceController, ParkingComplexDelegate {
    
    // MARK: Properties
    @IBOutlet var lastParkButton: WKInterfaceButton!
    @IBOutlet var lastParkLabel: WKInterfaceLabel!

    var markerController: MarkerController?


    // MARK: Functions
     override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)

        // Typically this will only happen on app launch or if the app is deep linked to
        // a controller deeper in the navigation stack, but this is OK because it is
        // the same object as the one that is passed to the next controller (if a future
        // controller were to create a brand new one, but this one was still retained by
        // this controller, we would have some data loss.
        if markerController == nil {
            markerController = MarkerController()
        }
    }

    override func willActivate() {
        super.willActivate()

        // Set the text on the top button so that it shows a few simple details about the latest park
        if let currentMarker = markerController!.currentMarker {
            lastParkLabel.setHidden(false)
            lastParkButton.setHidden(false)
            if currentMarker.endDate == nil {
                lastParkLabel.setText("CURRENT PARK")

                let dateFormatter = NSDateComponentsFormatter()
                dateFormatter.unitsStyle = .Abbreviated
                dateFormatter.allowedUnits = [.Day, .Hour, .Minute]
                dateFormatter.maximumUnitCount = 2
                lastParkButton.setTitle(dateFormatter.stringFromTimeInterval(NSDate().timeIntervalSinceDate(currentMarker.startDate))! + " ago")
            } else {
                lastParkLabel.setText("PREVIOUS PARK")

                let dateFormatter = NSDateFormatter()
                dateFormatter.timeStyle = .NoStyle
                dateFormatter.dateStyle = .ShortStyle
                dateFormatter.doesRelativeDateFormatting = true
                lastParkButton.setTitle(dateFormatter.stringFromDate(currentMarker.startDate))
            }
        } else {
            lastParkLabel.setHidden(true)
            lastParkButton.setHidden(true)
        }
    }

    override func contextForSegueWithIdentifier(segueIdentifier: String) -> AnyObject? {
        return markerController
    }

    @IBAction func newPark() {
        markerController!.newPark(floor: nil, row: nil)
        pushControllerWithName("Parked Controller", context: markerController)
    }

    @IBAction func showParkComplexModal() {
        // Show parking complex interface
        presentControllerWithName("Park Complex Controller", context: self)
    }

    // MARK: Parking Complex Delegate
    func parkingComplexAttributesSet(floor floor: Int, row: Int) {
        // Create a new Marker
        markerController!.newPark(floor: floor, row: row)
        pushControllerWithName("Parked Controller", context: markerController)
    }
}
